# COWMED 
### http://cowmed.com.br/en/
The company works with precision livestock. This work was developed to build / assist the monitoring of the service provided by the company.
Some access data was excluded from the code for security reasons.
---
## Project
This code accesses a NoSQL database (Elasticsearch) collecting information on the operating status of a farm. It also collects information such as location through an API generating a map with different layers and popups informing the status.

Only one screenshot of the generated HTML file was taken.